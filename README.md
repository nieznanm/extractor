# Extractor
A microservice for extracting features from audio data.

# Example use of the API in cURL:
## Status
```
curl localhost:5000/status
```
Response:
```
{"n_features":2,"trained":true}
```
```
{"n_features":-1,"trained":false}
```

## Train
```
curl --header "Content-Type: application/json" \
  --request POST \
  --data '[
      "https://freepd.com/music/City%20Sunshine.mp3",
      "https://freepd.com/music/Stereotype%20News.mp3",
      "https://freepd.com/music/Happy%20Whistling%20Ukulele.mp3",
      "https://freepd.com/music/Inspiration.mp3",
      "https://freepd.com/music/Advertising.mp3",
      "https://freepd.com/music/Be%20Chillin.mp3"]' \
  localhost:5000/train
```
Response:
```
{"task_id":"28420eea-43e0-43a0-9d70-231273ec9730"}
```

## Task status
```
curl localhost:5000/task_status/28420eea-43e0-43a0-9d70-231273ec9730
```

Response:
```
{"processed":4,"state":"PROGRESS","status":"Last processed url: https://www.youtube.com/audiolibrary_download?vid=b8c98f33c0762017","total":6}
```
```
{"processed":6,"state":"SUCCESS","status":"Completed","total":6}
```

## Extract features
```
curl -F "file=@example_data/Aminor.wav" localhost:5000/extract_features
```
Response:
```
[[-0.7989784150771102,-0.06167179972288813]]
```

# Installation
Install Python3, ffmpeg and Redis e.g.:
```
sudo pacman -S python ffmpeg redis
```
```
pip install -r requirements.txt
```

# Usage
Start Redis:
```
redis-server
```
Start the Extractor in development environment:
```
FLASK_APP=application.py FLASK_RUN_PORT=5000 flask run
```

Start celery (either in the backgroud or in the foreground):
Start celery in the in the background:
```
REDIS_HOST="localhost" REDIS_PORT="6379" celery worker -A application:celery --detach --logfile celery.log --loglevel INFO
```
Start celery in the in the foregound:
```
REDIS_HOST="localhost" REDIS_PORT="6379" celery worker -A application:celery
```

Start the Extractor in production environment using gunicorn
```
REDIS_HOST="localhost" REDIS_PORT="6379" gunicorn -w 4 -b 127.0.0.1:5000 wsgi:app --timeout 60
```
