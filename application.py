import flask
import logging
import os
from celery import Celery

import features
import flask_utils

logging.basicConfig(level=logging.INFO)

app = flask.Flask(__name__)

redis_host = os.environ.get('REDIS_HOST')
redis_port = os.environ.get('REDIS_PORT')
if redis_host is None:
    redis_host = 'localhost'
if redis_port is None:
    redis_port = 6379
else:
    redis_port = int(redis_port)

app.config['CELERY_BROKER_URL'] = 'redis://{host}:{port}/0'.format(host=redis_host, port=redis_port)
app.config['CELERY_RESULT_BACKEND'] = 'redis://{host}:{port}/0'.format(host=redis_host, port=redis_port)

celery = Celery(app.name, broker=app.config['CELERY_BROKER_URL'])
celery.conf.update(app.config)

extractor = features.Extractor(redis_host, redis_port)

@app.errorhandler(flask_utils.InvalidUsage)
def handle_invalid_usage(error):
    response = flask.jsonify(error.to_dict())
    response.status_code = error.status_code
    return response

@app.route('/extract_features', methods=['POST'])
def extract_features_endpoint():
    f = flask.request.files['file']
    try:
        fs = extractor.extract_features(
                extractor.read_audio_file(f)
                )
        return flask.jsonify(fs)
    except features.DataError as e:
        raise flask_utils.InvalidUsage(e.message)
    except features.StateError as e:
        raise flask_utils.InvalidUsage(e.message, status_code=409)

@app.route('/train', methods=['POST'])
def train_endpoint():
    urls = flask.request.get_json()
    task = train_task.apply_async(args=[urls])
    return flask.jsonify({"task_id" : task.id}), 202, {'Location': flask.url_for('task_status', task_id=task.id)}

@celery.task(bind=True)
def train_task(self, urls):
    def callback(processed, total, url):
        self.update_state(state='PROGRESS',
                meta={
                    "processed" : processed,
                    "total" : total,
                    "status" : "Last processed url: {url}".format(url=url)
                    }
                )
    processed, total = extractor.train(urls, processed_callback=callback)
    return {"processed" : processed, "total" : total, "status" : "Completed"}

@app.route('/task_status/<task_id>')
def task_status(task_id):
    task = train_task.AsyncResult(task_id)
    if task.state == 'PENDING':
        # job did not start yet
        response = {
            'state': task.state,
            'processed': 0,
            'total': 1,
            'status': 'Pending...'
        }
    elif task.state != 'FAILURE':
        response = {
            'state': task.state,
            'processed': task.info.get('processed', 0),
            'total': task.info.get('total', 1),
            'status': task.info.get('status', '')
        }
        if 'result' in task.info:
            response['result'] = task.info['result']
    else:
        # something went wrong in the background job
        response = {
            'state': task.state,
            'current': 1,
            'total': 1,
            'status': str(task.info),  # this is the exception raised
        }
    return flask.jsonify(response)

@app.route('/status', methods=['GET'])
def status_endpoint():
    status = extractor.status()
    return flask.jsonify(status)
