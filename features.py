import io
import logging
import numpy
import os
import pickle
import redis
import requests
import ffmpeg
import time
import matplotlib.pyplot as plt
import scipy.signal
from sklearn.decomposition import IncrementalPCA

class Error(Exception):

    def __init__(self, message=""):
        self.message = message

class DataError(Error):
    pass


class StateError(Error):
    pass


class Extractor:

    PCA_KEY = 'EXTRACTOR_PCA'
    TRAINED_KEY = 'EXTRACTOR_TRAINED'
    PROCESSED_KEY = 'EXTRACTOR_PROCESSED'


    def __init__(self,
            redis_host=None,
            redis_port=None,
            timeout=1,
            n_components=49):
        """
        timeout : float
            Wait for *timeout* seconds before downloading a file
            to avoid getting banned.
        """

        self._logger = logging.getLogger(__name__)

        self._timeout = timeout
        self._n_components = n_components

        self._state = {
                self.PCA_KEY : None,
                self.TRAINED_KEY : False,
                self.PROCESSED_KEY : []
                }
        self._redis = redis.Redis(host=redis_host, port=redis_port, db=0)
        self._load_state()

    def status(self):
        self._load_state()
        if self._state[self.TRAINED_KEY]:
            return {"trained" : True, "n_features" : self._state[self.PCA_KEY].n_components}
        else:
            return {"trained" : False, "n_features" : -1}

    def _load_state(self):
        """
        Load the state shared accross the processes from Redis.
        """
        serialized_pca = self._redis.get(self.PCA_KEY)
        if serialized_pca is None:
            self._state[self.PCA_KEY] = None
            self._state[self.TRAINED_KEY] = False
        else:
            pca = pickle.loads(serialized_pca)
            self._state[self.PCA_KEY] = pca
            self._state[self.TRAINED_KEY]= pca is not None
            self._logger.info("PCA loaded from Redis")

        serialized_processed = self._redis.get(self.PROCESSED_KEY)
        if serialized_processed is not None:
            self._state[self.PROCESSED_KEY] = pickle.loads(serialized_processed)
            self._logger.info("Processed features loaded from Redis")

    def _save_state(self):
        """
        Save the state shared accross the processes to Redis.
        """
        serialized_pca = pickle.dumps(self._state[self.PCA_KEY])
        self._redis.set(self.PCA_KEY, serialized_pca)

        if serialized_pca is None:
            self._state[self.PCA_KEY] = None
            self._state[self.TRAINED_KEY] = False
        else:
            self._state[self.PCA_KEY] = numpy.loads(serialized_pca)
            self._state[self.TRAINED_KEY]= True

        serialized_processed = pickle.dumps(self._state[self.PROCESSED_KEY])
        self._redis.set(self.PROCESSED_KEY, serialized_pca)

    def extract_basic_features(self,
            data,
            samples_per_batch=44100,
            frequency_range=slice(100, 4000)
            ):
        """
        Returns
        -------
        features : iterable with numerical features
        """

        n_batches = data.shape[0] // samples_per_batch
        if n_batches < 1:
            raise DataError("Sample too short")


        data = data.astype(numpy.float32)
        std = numpy.std(data)
        mean = numpy.mean(data)
        # Normalize data
        data = (data - mean) / std

        batches = data[:n_batches * samples_per_batch].reshape(
                n_batches, samples_per_batch)

        dft = numpy.fft.rfft(batches, axis=1)
        amplitude = numpy.abs(dft)

        #mean_amplitude = numpy.mean(amplitude, axis=0)
        #filtered_mean_amplitude = mean_amplitude[frequency_range]

        #argmax_filtered_mean_amplitude = numpy.argmax(filtered_mean_amplitude)
        #max_mean_amplitude = filtered_mean_amplitude[argmax_filtered_mean_amplitude]
        #normalized_filtered_mean_amplitude = mean_amplitude / max_mean_amplitude

        #normalized_argmax_filtered_mean_amplitude = argmax_filtered_mean_amplitude / (filtered_mean_amplitude.shape[0] - 1)

        #features = numpy.concatenate([
        #    normalized_filtered_mean_amplitude,
        #    [normalized_argmax_filtered_mean_amplitude]
        #    ], axis=0)

        max_amplitude = numpy.mean(amplitude, axis=0)
        filtered_max_amplitude = max_amplitude[frequency_range]
        
        step = 5
        nma = filtered_max_amplitude[::step]
        nma = (nma - numpy.mean(nma)) / numpy.std(nma)
        dfmax = numpy.diff(filtered_max_amplitude[::step])
        dfmax_mean = numpy.mean(dfmax)
        dfmax_std = numpy.std(dfmax)
        dfmax = (dfmax - dfmax_mean) / dfmax_std

        energy = data**2

        window_size = 2*44100
        test_window = energy[:-window_size]
        test_domain = energy
        cor = scipy.signal.fftconvolve(test_domain, test_window[::-1], mode="valid")
        smooth_cor = scipy.signal.savgol_filter(cor, 1001, 3)
        skip_first = 12000
        skip_last = 12000
        argmax_cor = numpy.argmax(smooth_cor[skip_first:-skip_last])
        normalized_tempo = (skip_first + argmax_cor) / (window_size - skip_last)
        tempo_weight = 5
        tempo = normalized_tempo * tempo_weight
        #print(tempo)
        #print(argmax_cor)
        #plt.plot(cor)
        #plt.plot(smooth_cor)
        #plt.show()
        #plt.plot(energy)
        #plt.show()

        #features = dfmax
        features = numpy.concatenate([
            dfmax,
            nma,
            [tempo]
            ], axis=0)

        return features

    def extract_features(self, audio_file):
        self._load_state()
        if not self._state[self.TRAINED_KEY]:
            raise StateError("Attempting to extract features from untrained model")
        bf = self.extract_basic_features(audio_file)
        ff = self._state[self.PCA_KEY].transform(bf.reshape(1,- 1))
        return ff.tolist()

    def read_audio_file(self, f):
        return self.download_audio('-', input=f.read())

    def download_audio(self, url, input=None):
        self._logger.info("Downloading: {}".format(url))
        try:
            out, _ = (ffmpeg
                .input(url)
                .output('-', format='s16le', acodec='pcm_s16le', ac=1, ar=44100)
                .overwrite_output()
                .run(capture_stdout=True, input=input)
            )
        except ffmpeg.Error:
            raise DataError("Invalid audio file")

        data = numpy.frombuffer(out, dtype=numpy.int16)
        return data

    def _ensure_execution(self, f, *args, max_tries=5, timeout=2.0, **kwargs):
        for i in range(max_tries):
            try:
                return f(*args, **kwargs)
            except Exception as e:
                if i != max_tries - 1:
                    self._logger.info("Function failed with Exception {}. Retrying in {} seconds".format(e, timeout))
                    time.sleep(timeout)
        raise e

    def train(self,
            urls,
            batch_size=500,
            processed_callback=lambda processed, total, url : None):

        len_urls = len(urls)
        if len_urls < batch_size:
            batch_sizes = [len_urls]
        else:
            n_batches = len_urls // batch_size
            # It's better to have a bigger batch than a smaller one.
            # So if len(urls) != batch_size * K make the last batch larger.
            batch_sizes = [batch_size] * (n_batches - 1) + [batch_size + len(urls) % batch_size]

        total = len(urls)
        n_components = min(total - 1, self._n_components)

        self._logger.info("Initializing PCA with {} components".format(n_components))
        ipca = IncrementalPCA(n_components=n_components, batch_size=batch_size)

        processed = 0
        beg = 0
        for bs in batch_sizes:
            bfs = []
            for url in urls[beg:beg+bs]:
                processed += 1
                processed_callback(processed, total, url)
                try:
                    a = self._ensure_execution(self.download_audio, url)
                except:
                    continue
                bf = self.extract_basic_features(a)
                bfs.append(bf)
            abfs = numpy.array(bfs)
            self._logger.info("Fitting batch with size {}".format(len(bfs)))
            ipca.partial_fit(abfs)
            beg += bs
        
        self._state[self.PCA_KEY] = ipca
        self._state[self.TRAINED_KEY] = True
        self._save_state()

        return processed, total
